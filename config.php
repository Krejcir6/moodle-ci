<?php  // Moodle configuration file
unset($CFG);
global $CFG;
$CFG = new stdClass();
$CFG->dbtype    = 'pgsql';
$CFG->dblibrary = 'native';
$CFG->dbhost    = 'pg';
$CFG->dbname    = 'test';
$CFG->dbuser    = 'test';
$CFG->dbpass    = 'test';
$CFG->prefix    = 'mdl_';
$CFG->dboptions = array (
 'dbpersist' => 0,
 'dbport' => '5432',
 'dbsocket' => '',
//  'logall'=> true,
);

$CFG->dataroot  = __DIR__ .'/test_dataroot';
$CFG->wwwroot = 'https://localhost';
$CFG->admin     = 'admin';
//ini_set('memory_limit','512M');
//@error_reporting(E_ALL | E_STRICT); // NOT FOR PRODUCTION SERVERS!
//@ini_set('display_errors', '1');    // NOT FOR PRODUCTION SERVERS!
//$CFG->debug = (E_ALL | E_STRICT);   // === DEBUG_DEVELOPER - NOT FOR PRODUCTION SERVERS!
//$CFG->debugdisplay = 1;             // NOT FOR PRODUCTION SERVERS!
$CFG->cachejs = false;
$CFG->directorypermissions = 0777;
$CFG->langstringcache   = false;


$CFG->noemailever = true;


require_once(__DIR__ . '/lib/setup.php');

// There is no php closing tag in this file,
// it is intentional because it prevents trailing whitespace problems!

/* ************************************************************************** */
/* PDC nastaveni */
/* ************************************************************************** */

// aby se dala local rozsireni pretezovat v zakaznickem local rozsireni
$CFG->phpunit_prefix = 'phpu_';
$CFG->phpunit_dataroot = $CFG->dataroot.'/test';
$CFG->phpunit_dboptions =  [];
$CFG->langstringcache = false;


/* ************************************************************************** */
